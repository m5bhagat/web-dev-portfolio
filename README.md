This is my Web Design and Development Portfolio. Below are a short description of some of the sites I have made.

Each website is stored in a separate folder. Below are the names of each website's corresponding folder, and a brief description about it.

------------------------------------------------------------------------------------------------------------------------------------------------------------
reactJS-nature:
I was interested in learning ReactJS, so I thought making a website would be a great way to start. It is a pretty simple website. The design is pretty basic and it features a bunch of nature photos. I made it using Reactstrap and Bootstrap. This project was a great way for me to get started with React, but there are more ReactJS projects to come.

------------------------------------------------------------------------------------------------------------------------------------------------------------
Meteor:
This is a website is made using only HTML and CSS. I used an online template as a visual reference, but all the code is my own work. Almost the entire website is responsive, except for a few components which were too much of a hassle to make responsive.

------------------------------------------------------------------------------------------------------------------------------------------------------------
RKBv2:
This is a currently published site, viewable at RKBaccounting.ca. I made it for an accounting firm. It was designed by me, and Adhere's to Google's Material Design guidelines. I implemented the website using the Materialize library, with php as a backend. The backend is pretty simple, it's just to accept form submissions (Consultation requests to be exact). However, instead of storing the date submitted through the form in a database it is emailed to someone.

For the business in general, I also performed some digital marketing. I performed SEO for the site, setup an adwords account, did some basic link building by posting on web directories and provided reccomendations for the future of the firm's marketing.

------------------------------------------------------------------------------------------------------------------------------------------------------------
RKBv1:
This is the first site I made for the same business as above. I threw it together over a few days, to get started quickly, but it was always meant to be a placeholder. What's cool about this website, is that it is made using pure HTML and CSS.

------------------------------------------------------------------------------------------------------------------------------------------------------------
personal-website:
This is a website I had intended to be my personal website, but I have halted further development for now. I had intended to use it to host my projects and my resume. However, I find it much more efficient and convenient to host my projects on Github. I'm still trying to figure out what to use this domain for; maybe I'll start a blog about Digital Marketing, which is a secondary interest of mine. This website was designed and developed by me, and like many of the others on this list, is also made using pure HTML and CSS.

------------------------------------------------------------------------------------------------------------------------------------------------------------
lawfirm:
This site is probably one of my earliest websites. One which I also made for practice. It is made using pure HTML and CSS. What' cool about it is that it is completely responsive. It's a pretty small site, but was a great learning opportunity.